#!/bin/sh

# Copy ssh key:

adb push ~/.ssh/id_rsa.pub /data/ssh/authorized_keys

adb shell chmod 600 /data/ssh/authorized_keys

# Create directories:
adb shell busybox mkdir -p /data/local/userinit.d

adb push sshd-cm-10.1.x-template-0 /data/local/userinit.d/90sshd
adb shell busybox chmod +x /data/local/userinit.d/90sshd

adb push sshd-cm-10.1.x-template-1 /data/ssh/sshd_config
adb shell chmod 644 /data/ssh/sshd_config

echo "Now either reboot or execute /data/local/userinit.d/90sshd."

