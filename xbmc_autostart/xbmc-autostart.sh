#!/bin/sh

# Create directories:
adb shell busybox mkdir -p /data/local/userinit.d

adb shell busybox mkdir -p /data/local/bin

# Copy template script:
adb push xbmc-autostart-template-0 /data/local/userinit.d/91xbmc

adb push xbmc-autostart-template-1 /data/local/bin/delayed-xbmc-start

adb shell busybox chmod +x /data/local/bin/delayed-xbmc-start
adb shell busybox chmod +x /data/local/userinit.d/91xbmc

echo "Now either reboot or execute /data/local/userinit.d/91xbmc."

