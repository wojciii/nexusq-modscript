#!/bin/bash

pushd nightlies
LAST=`ls -Art | tail -n 1`
popd

adb install -r nightlies/$LAST
