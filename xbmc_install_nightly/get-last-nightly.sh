#!/bin/bash

#URL="http://mirrors.xbmc.org/nightlies/android/arm/?C=M;O=D"
#URL="rsync://ftp.sunet.se/pub/multimedia/xbmc/"
URL="rsync://mirror.de.leaseweb.net/xbmc"

DATE=`date -d yesterday +%Y%m%d`
#DATE=`date -d "two days ago" +%Y%m%d`

mkdir -p nightlies
pushd nightlies 
rsync -rv $URL/nightlies/android/arm/xbmc-$DATE*.apk .
popd
