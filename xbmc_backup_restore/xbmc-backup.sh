#!/bin/bash

DATESTAMP=`date +%s`

OUTPUT="backup-${DATESTAMP}/data/org.xbmc.xbmc/files/.xbmc/userdata"
FILESROOT="/sdcard/Android/data/org.xbmc.xbmc/files/.xbmc/userdata"

mkdir -p ${OUTPUT}

pushd ${OUTPUT}

# Config files to backup:
adb pull $FILESROOT/LCD.xml .
adb pull $FILESROOT/RssFeeds.xml .
adb pull $FILESROOT/guisettings.xml .
adb pull $FILESROOT/profiles.xml .
adb pull $FILESROOT/sources.xml .

# Database directory to backup, not sure if 
# this is required.
mkdir -p Database
pushd Database
adb pull $FILESROOT/Database/ .
popd > /dev/null

popd > /dev/null

tar czf backup-${DATESTAMP}.tar.bz2 backup-${DATESTAMP} && \
rm -fr backup-${DATESTAMP} && \
echo "Backup complete."
